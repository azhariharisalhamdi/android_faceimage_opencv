#include "jni.h"

#ifndef __ID_IMAGE_PROCESSING__
#define	__ID_IMAGE_PROCESSING__

JNIEXPORT jlong JNICALL Java_id_co_marlonamadeus_secureguestbook_DetectionBasedTracker_findSquares(JNIEnv * jenv, jclass, jlong thiz, jlong image, jlong squares);
JNIEXPORT jlong JNICALL Java_id_co_marlonamadeus_secureguestbook_DetectionBasedTracker_drawSquares(JNIEnv * jenv, jclass, jlong thiz, jlong image, jlong squares);


#endif