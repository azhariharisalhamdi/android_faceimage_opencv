package id.co.marlonamadeus.secureguestbook;

import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.widget.TextView;

public class testActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        Intent intent = getIntent();
        String mfilePict = intent.getStringExtra(MainActivity.PICT_MESSAGE);
        TextView textView = (TextView) findViewById(R.id.textView);
        textView.setText(mfilePict);
    }

}