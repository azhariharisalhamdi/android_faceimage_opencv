package id.co.marlonamadeus.secureguestbook;

import android.annotation.SuppressLint;
import android.app.Activity;
//import android.support.v7.app.ActionBarActivity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfDouble;
import org.opencv.core.MatOfInt;
import org.opencv.core.MatOfRect;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
//import org.opencv.core.Size;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;
import org.opencv.objdetect.CascadeClassifier;
import org.opencv.imgproc.Imgproc;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Vector;

import static java.lang.Math.sqrt;
import static org.opencv.core.Core.LINE_4;
import static org.opencv.core.Core.LINE_AA;
import static org.opencv.core.Core.mixChannels;
import static org.opencv.core.CvType.CV_64F;
import static org.opencv.core.CvType.CV_8U;
import static org.opencv.imgproc.Imgproc.polylines;
import static org.opencv.imgproc.Imgproc.pyrDown;
import static org.opencv.imgproc.Imgproc.pyrUp;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.hardware.Camera.Size;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;

import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SubMenu;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Mat;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;

public class IDImageProcessing extends Activity implements CvCameraViewListener2, OnTouchListener
{
    private static final String TAG = "ID_OPENCV";

    private screenpicture mOpenCvCameraView;

    static int N = 12;

//    private CameraBridgeViewBase                mOpenCvCameraView;
    private Mat                                 mRgba = new Mat();
    private Mat                                 mGray = new Mat();
    private Mat                                 bgrPixel = new Mat();
    java.util.List<org.opencv.core.MatOfPoint>  squaresObj = new ArrayList<org.opencv.core.MatOfPoint>();

    double getFocus;

    TextView textView;

    boolean hasSave = false;

    private ImageView showPhoto;

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                {
                    Log.i("ID_OPENCV", "OpenCV loaded successfully");
                    if(hasSave) {
                        if (mOpenCvCameraView != null)
                            mOpenCvCameraView.disableView();
                        Log.d(TAG, "masuk di hassave");
                    }
                    else {
                        mOpenCvCameraView.enableView();
                        Log.d(TAG, "masuk di not save");
                    }
//                    textView.setText("blur Score: "+getFocus);
                    mOpenCvCameraView.setOnTouchListener(IDImageProcessing.this);
                } break;
                default:
                {
                    super.onManagerConnected(status);
                } break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        setContentView(R.layout.activity_idimage_processing);

        //mOpenCvCameraView = (CameraBridgeViewBase) findViewById(R.id.id_activity_surface_view);

        if(hasSave)
            showPhoto = (ImageView) findViewById(R.id.id_activity_surface_view);
        else
            mOpenCvCameraView = (screenpicture) findViewById(R.id.id_activity_surface_view);

        textView = (TextView) findViewById(R.id.viewFocus);

//        textView.setText("blur Score: "+getFocus);
        Log.d(TAG, "masuk di create");
        mOpenCvCameraView.setVisibility(CameraBridgeViewBase.VISIBLE);
        mOpenCvCameraView.setCvCameraViewListener(this);
    }

    void calculateBlur(Mat images) throws  Exception
    {
        new blurCalculation().execute(images);
    }

    private class blurCalculation extends AsyncTask<Mat, Double, String> {
        @Override
        protected String doInBackground(Mat... images) {
//            if(images[0] == null)
//                return  null;
//          while()
            {
                Mat image = images[0];
                getFocus = bluryMeasurement(image);
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Double... values) {
            super.onProgressUpdate(values);
            textView.append("update" + getFocus);
        }
    }

    @SuppressLint("SimpleDateFormat")
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        Log.i(TAG,"onTouch event");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
        String currentDateandTime = sdf.format(new Date());
        String fileName = Environment.getExternalStorageDirectory().getPath() +
                "/user_pict" + ".jpg";
        mOpenCvCameraView.takePicture(fileName);
        Toast.makeText(this, fileName + " saved", Toast.LENGTH_SHORT).show();
        hasSave = true;
        return false;
    }



    static double angle( Point pt1, Point pt2, Point pt0 )
    {
        double dx1 = pt1.x - pt0.x;
        double dy1 = pt1.y - pt0.y;
        double dx2 = pt2.x - pt0.x;
        double dy2 = pt2.y - pt0.y;
        return (dx1*dx2 + dy1*dy2)/sqrt((dx1*dx1 + dy1*dy1)*(dx2*dx2 + dy2*dy2) + 1e-10);
    }

    void findSquares( Mat image, java.util.List<org.opencv.core.MatOfPoint> squares )
    {
        if( squares != null && !squares.isEmpty() )
            squares.clear();
        Mat smallerImg=new Mat(new org.opencv.core.Size(image.width()/2, image.height()/2),image.type());
        Mat gray=new Mat(image.size(),image.type());
        Mat gray0=new Mat(image.size(),org.opencv.core.CvType.CV_8U);
        Imgproc.pyrDown(image, smallerImg, smallerImg.size());
        Imgproc.pyrUp(smallerImg, image, image.size());

        for( int c = 0; c < 3; c++ )
        {
            extractChannel(image, gray, c);
            for( int l = 1; l < N; l++ )
            {
                Imgproc.threshold(gray, gray0, (l+1)*255/N, 255, Imgproc.THRESH_BINARY);
                java.util.List<org.opencv.core.MatOfPoint> contours = new java.util.ArrayList<org.opencv.core.MatOfPoint>();
                Imgproc.findContours(gray0, contours, new Mat(), Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);

                org.opencv.core.MatOfPoint approx  =   new org.opencv.core.MatOfPoint();
                for( int i = 0; i < contours.size(); i++ )
                {

                    approx = approxPolyDP(contours.get(i),  Imgproc.arcLength(new org.opencv.core.MatOfPoint2f(contours.get(i).toArray()), true)*0.02, true);

                    if( approx.toArray().length == 4 &&
                            Math.abs(Imgproc.contourArea(approx)) > 1000 &&
                            Imgproc.isContourConvex(approx) )
                    {
                        double maxCosine = 0;

                        for( int j = 2; j < 5; j++ )
                        {
                            // find the maximum cosine of the angle between joint edges
                            double cosine = Math.abs(angle(approx.toArray()[j%4], approx.toArray()[j-2], approx.toArray()[j-1]));
                            maxCosine = Math.max(maxCosine, cosine);
                        }

                        if( maxCosine < 0.2 ) //0.3
                            squares.add(approx);
                    }
                }
            }
        }
    }

    void extractChannel(Mat source, Mat out, int channelNum)
    {
        java.util.List<Mat> sourceChannels = new java.util.ArrayList<Mat>();
        java.util.List<Mat> outChannel = new java.util.ArrayList<Mat>();
        org.opencv.core.Core.split(source, sourceChannels);
        outChannel.add(new Mat(sourceChannels.get(0).size(),sourceChannels.get(0).type()));
        org.opencv.core.Core.mixChannels(sourceChannels, outChannel, new MatOfInt(channelNum,0));
        org.opencv.core.Core.merge(outChannel, out);
    }

    org.opencv.core.MatOfPoint approxPolyDP(org.opencv.core.MatOfPoint curve, double epsilon, boolean closed)
    {
        org.opencv.core.MatOfPoint2f tempMat=new org.opencv.core.MatOfPoint2f();

        Imgproc.approxPolyDP(new org.opencv.core.MatOfPoint2f(curve.toArray()), tempMat, epsilon, closed);

        return new org.opencv.core.MatOfPoint(tempMat.toArray());
    }

    static void drawSquares( Mat image, java.util.List<org.opencv.core.MatOfPoint> squares ) {
        for (long i = 0; i < squares.size(); i++) {
//            Point p = squares[i][0];
//            int n = (int) squares[i].size();
            polylines(image, squares,true,  new Scalar(0, 255, 0), LINE_4);
        }
    }

    static double bluryMeasurement(Mat rgbVal)
    {
        Mat grayTemp = new Mat();

//        grayTemp = rgbVal;
        Imgproc.cvtColor(rgbVal,grayTemp,Imgproc.COLOR_RGB2GRAY,1);

        Imgproc.Laplacian(grayTemp, grayTemp, CV_64F);
        MatOfDouble median = new MatOfDouble();
        MatOfDouble std= new MatOfDouble();
        Core.meanStdDev(grayTemp, median , std);
        return Math.pow(std.get(0,0)[0], 2);
    }

    double laplacianFocus(Mat rgbVal)
    {
        return 0;
    }

    @Override
    public void onResume()
    {
        super.onResume();
        if (!OpenCVLoader.initDebug()) {
            Log.d(TAG, "Internal OpenCV library not found. Using OpenCV Manager for initialization");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_4_0, this, mLoaderCallback);
        } else {
            Log.d(TAG, "OpenCV library found inside package. Using it!");
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }
    }

    @Override
    public void onPause()
    {
        super.onPause();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    @Override
    public Mat onCameraFrame(CvCameraViewFrame inputFrame) {


        mGray = inputFrame.gray();
        if(mRgba == null) {
            mRgba = inputFrame.rgba();
            bgrPixel = mRgba.submat(1, 1, 2, 2).clone();
        }
        findSquares(bgrPixel, squaresObj);
        drawSquares(bgrPixel, squaresObj);

//        getFocus = bluryMeasurement(mRgba);
//        textView.append("update" + getFocus);

        try {
            calculateBlur(bgrPixel);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return bgrPixel;
    }

    @Override
    public void onCameraViewStarted(int width, int height) {
        mGray = new Mat();
        mRgba = new Mat();
    }

    @Override
    public void onCameraViewStopped() {
        mGray.release();
        mRgba.release();
    }

    public void onDestroy() {
        super.onDestroy();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    /**
     * Called when a touch event is dispatched to a view. This allows listeners to
     * get a chance to respond before the target view.
     *
     * @param v     The view the touch event has been dispatched to.
     * @param event The MotionEvent object containing full information about
     *              the event.
     * @return True if the listener has consumed the event, false otherwise.
     */
}