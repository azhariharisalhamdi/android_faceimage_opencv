package id.co.marlonamadeus.secureguestbook;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

//public class MainActivity extends Activity implements OnClickListener
public class MainActivity extends Activity
//public class MainActivity extends AppCompatActivity
{

	private ImageView mImageView;
	private static final String TAG = "upload";
    static final int REQUEST_IMAGE_CAPTURE = 1;
    private Uri photoURI;
    static final int REQUEST_TAKE_PHOTO = 1;
    public String mCurrentPhotoPath;
	public static final String PICT_MESSAGE = "User_Face_Photo";

	public Button mTakePhoto;
	public Button goToAnotherScreen;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);


		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);

//		mTakePhoto = (Button) findViewById(R.id.take_photo);
		mImageView = (ImageView) findViewById(R.id.imageview);
//		goToAnotherScreen = (Button) findViewById(R.id.gotoanother);
//		if (android.os.Build.VERSION.SDK_INT > 23) {
//			if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
//				ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.INTERNET}, 0);
//			}
//		}
//		else {
//
//		}
//
//		Context context;
//		int permission = PermissionChecker.checkSelfPermission(context, permission);
//
//		if (permission == PermissionChecker.PERMISSION_GRANTED) {
//			// good to go
//		} else {
//			// permission not granted, you decide what to do
//		}


	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

//	@Override
//	public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
//		if (requestCode == 0) {
//			if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
//					&& grantResults[1] == PackageManager.PERMISSION_GRANTED) {
//				mTakePhoto.setEnabled(true);
//			}
//		}
//	}


	public void dispatchTakePictureIntent(View view)
//	private void dispatchTakePictureIntent()
	{
		Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
			File photoFile = null;
			try {
				photoFile = createImageFile();
			} catch (IOException ex) {
			}
			if (photoFile != null) {
                photoURI = FileProvider.getUriForFile(this,"id.co.marlonamadeus.secureguestbook.fileprovider",photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
				startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
//				setPic();
			}
		}
	}

//	public void gotoAnotherActivity(View view)
	public void gotoAnotherActivity()
	{
//		Intent intent = new Intent(this, FaceDetection.class);
//		Intent intent = new Intent(this, ImageProcessing.class);
//		Intent intent = new Intent(this, FdActivity.class);
//		Intent intent = new Intent(this, testDetection.class);
//		Intent intent = new Intent(this, Login.class);
//		Intent intent = new Intent(this, Tutorial1Activity.class);
//		Intent intent = new Intent(this, CameraPreview.class);
		Intent intent = new Intent(this, IDImageProcessing.class);
		intent.putExtra(PICT_MESSAGE, mCurrentPhotoPath);
		startActivity(intent);
	}

	public void galleryAddPic() {
		Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
		File f = new File(mCurrentPhotoPath);
		Uri contentUri = Uri.fromFile(f);
		mediaScanIntent.setData(contentUri);
		this.sendBroadcast(mediaScanIntent);
	}

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",java.util.Locale.getDefault()).format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == REQUEST_IMAGE_CAPTURE) {
			if (resultCode == RESULT_OK) {
				mImageView.setImageURI(photoURI);
//				setPic();
				gotoAnotherActivity();
			}
		}
	}

	public void gotoFaceTest(View view)
	{
		Intent intent = new Intent(this, FdActivity.class);
		startActivity(intent);
	}

	public void gotoIDTest(View view)
	{
		Intent intent = new Intent(this, IDImageProcessing.class);
		startActivity(intent);
	}

	public void sendPhoto(Bitmap bitmap) throws Exception {
		new UploadTask().execute(bitmap);
	}

	public class UploadTask extends AsyncTask<Bitmap, Void, Void> {

		protected Void doInBackground(Bitmap... bitmaps) {
			if (bitmaps[0] == null)
				return null;
			setProgress(0);

			Bitmap bitmap = bitmaps[0];
			ByteArrayOutputStream stream = new ByteArrayOutputStream();
			bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream); // convert Bitmap to ByteArrayOutputStream
			InputStream in = new ByteArrayInputStream(stream.toByteArray()); // convert ByteArrayOutputStream to ByteArrayInputStream

			DefaultHttpClient httpclient = new DefaultHttpClient();
//			HttpClient httpclient = HttpClientBuilder.create().build();
			try {
				HttpPost httppost = new HttpPost("http://geco.transparansi.xyz/savetofile.php"); // server

				MultipartEntity reqEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
				reqEntity.addPart("myFile",
						System.currentTimeMillis() + ".jpg", in);
				httppost.setEntity(reqEntity);

				HttpResponse response = null;
				try {
					response = httpclient.execute(httppost);
				} catch (ClientProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {
					if (response != null)
						Log.i(TAG, "response " + response.getStatusLine().toString());
				} finally {

				}
			} finally {

			}

			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			if (stream != null) {
				try {
					stream.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			return null;
		}

		@Override
		protected void onProgressUpdate(Void... values) {
			// TODO Auto-generated method stub
			super.onProgressUpdate(values);
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			Toast.makeText(MainActivity.this, R.string.uploaded, Toast.LENGTH_LONG).show();
		}
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		Log.i(TAG, "onResume: " + this);
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		Log.i(TAG, "onSaveInstanceState");
	}

	private void setPic()
//	public void setPic(View view)
	{
		// Get the dimensions of the View
	    int targetW = mImageView.getWidth();
	    int targetH = mImageView.getHeight();

	    // Get the dimensions of the bitmap
	    BitmapFactory.Options bmOptions = new BitmapFactory.Options();
	    bmOptions.inJustDecodeBounds = true;
	    BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
	    int photoW = bmOptions.outWidth;
	    int photoH = bmOptions.outHeight;

	    // Determine how much to scale down the image
	    int scaleFactor = Math.min(photoW/targetW, photoH/targetH);

	    // Decode the image file into a Bitmap sized to fill the View
	    bmOptions.inJustDecodeBounds = false;
	    bmOptions.inSampleSize = scaleFactor << 1;
	    bmOptions.inPurgeable = true;

	    Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);

//	    Matrix mtx = new Matrix();
//	    mtx.postRotate(90);
//	    // Rotating Bitmap
//	    Bitmap rotatedBMP = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), mtx, true);

	    if (bitmap != bitmap)
	    	bitmap.recycle();

	    mImageView.setImageBitmap(bitmap);

	    try {
			sendPhoto(bitmap);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
