package id.co.marlonamadeus.secureguestbook;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.impl.client.DefaultHttpClient;
import org.opencv.android.*;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;
import org.opencv.core.*;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import static org.opencv.objdetect.Objdetect.CASCADE_SCALE_IMAGE;

//public class FaceDetection extends Activity implements CvCameraViewListener2
public class FaceDetection extends Activity
{
    public String mfilePict;
    public Bitmap faceUser;
    private ImageView mImageView;

    public Mat faceImg;

    public Bitmap pictbitmap;

    public Mat imageMat;



    private CameraBridgeViewBase openCvCameraView;
    private CascadeClassifier cascadeClassifier;

    private int absoluteFaceSize;
    private static final String    TAG                 = "OCVSample::Activity";

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                    initializeOpenCVDependencies();
                    imageMat = new Mat();

                    detect();

                    break;
                default:
                    super.onManagerConnected(status);
                    break;
            }
        }
    };

//    private void initializeOpenCVDependencies() {
//
//        try {
//            // Copy the resource into a temp file so OpenCV can load it
//            InputStream is = getResources().openRawResource(R.raw.lbpcascade_frontalface);
//            File cascadeDir = getDir("cascade", Context.MODE_PRIVATE);
//            File mCascadeFile = new File(cascadeDir, "lbpcascade_frontalface.xml");
//            FileOutputStream os = new FileOutputStream(mCascadeFile);
//
//
//            byte[] buffer = new byte[4096];
//            int bytesRead;
//            while ((bytesRead = is.read(buffer)) != -1) {
//                os.write(buffer, 0, bytesRead);
//            }
//            is.close();
//            os.close();
//
//            // Load the cascade classifier
//            cascadeClassifier = new CascadeClassifier(mCascadeFile.getAbsolutePath());
//        } catch (Exception e) {
//            Log.e("OpenCVActivity", "Error loading cascade", e);
//        }
//
//        // And we are ready to go
//        openCvCameraView.enableView();
//    }

    public void initializeOpenCVDependencies()
    {

        try {
            // Copy the resource into a temp file so OpenCV can load it
            InputStream is = getResources().openRawResource(R.raw.lbpcascade_frontalface);
            File cascadeDir = getDir("cascade", Context.MODE_PRIVATE);
            File mCascadeFile = new File(cascadeDir, "lbpcascade_frontalface.xml");
            FileOutputStream os = new FileOutputStream(mCascadeFile);


            byte[] buffer = new byte[4096];
            int bytesRead;
            while ((bytesRead = is.read(buffer)) != -1) {
                os.write(buffer, 0, bytesRead);
            }
            is.close();
            os.close();

            // Load the cascade classifier
            cascadeClassifier = new CascadeClassifier(mCascadeFile.getAbsolutePath());
            cascadeClassifier.load( mCascadeFile.getAbsolutePath() );
            if (cascadeClassifier.empty()) {
                Log.e(TAG, "Failed to load cascade classifier");
                cascadeClassifier = null;
            } else
                Log.i(TAG, "Loaded cascade classifier from " + mCascadeFile.getAbsolutePath());
        } catch (Exception e) {
            Log.e("OpenCVActivity", "Error loading cascade", e);
        }

        // And we are ready to go
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_face_detection);
//        setContentView(R.layout.face_detected);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);



//        openCvCameraView = new JavaCameraView(this, -1);
//        setContentView(openCvCameraView);
//        openCvCameraView.setCvCameraViewListener(this);

        Intent intent = getIntent();
        mfilePict = intent.getStringExtra(MainActivity.PICT_MESSAGE);

        faceUser = BitmapFactory.decodeFile(mfilePict);

        mImageView = (ImageView) findViewById(R.id.faceDetectedView);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

//        faceUser = bmp.copy(Bitmap.Config.ARGB_8888, true);
//        Utils.bitmapToMat(faceUser, faceImg);

//        mImageView.setImageBitmap(faceUser);
//        detect();
    }

    public void onResume()
    {
        super.onResume();
        if (!OpenCVLoader.initDebug()) {
            Log.d("OpenCV", "Internal OpenCV library not found. Using OpenCV Manager for initialization");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_4_0, this, mLoaderCallback);
        } else {
            Log.d("OpenCV", "OpenCV library found inside package. Using it!");
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }
    }

//    @Override
//    public void onCameraViewStarted(int width, int height) {
//        grayscaleImage = new Mat(height, width, CvType.CV_8UC4);
//
//        // The faces will be a 20% of the height of the screen
//        absoluteFaceSize = (int) (height * 0.2);
//    }
//
//    @Override
//    public void onCameraViewStopped() {
//    }

    /**
     * This method is invoked when delivery of the frame needs to be done.
     * The returned values - is a modified frame which needs to be displayed on the screen.
     * TODO: pass the parameters specifying the format of the frame (BPP, YUV or RGB and etc)
     *
     * @param inputFrame
     */
//    @Override
//    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {
//        return null;
//    }

//    public Mat faceDetetion(Mat img)
//    {
//
//        img = Imgcodecs.imread(mfilePict);
//        Imgproc.cvtColor(img, grayscaleImage, Imgproc.COLOR_RGBA2RGB);
//
//        MatOfRect faces = new MatOfRect();
//
//        // Use the classifier to detect faces
//        if (cascadeClassifier != null) {
//            cascadeClassifier.detectMultiScale(grayscaleImage, faces, 1.1, 2, 2,
//                    new Size(absoluteFaceSize, absoluteFaceSize), new Size());
//        }
//
//        // If there are any faces found, draw a rectangle around it
//        Rect[] facesArray = faces.toArray();
//        for (int i = 0; i <facesArray.length; i++)
//            Imgproc.rectangle(img, facesArray[i].tl(), facesArray[i].br(), new Scalar(0, 255, 0, 255), 3);
//
//        return img;
//    }

//    @Override
//    public Mat onCameraFrame(Mat aInputFrame) {
//        // Create a grayscale image
//        Imgproc.cvtColor(aInputFrame, grayscaleImage, Imgproc.COLOR_RGBA2RGB);
//
//        MatOfRect faces = new MatOfRect();
//
//        // Use the classifier to detect faces
//        if (cascadeClassifier != null) {
//            cascadeClassifier.detectMultiScale(grayscaleImage, faces, 1.1, 2, 2,
//                    new Size(absoluteFaceSize, absoluteFaceSize), new Size());
//        }
//
//        // If there are any faces found, draw a rectangle around it
//        Rect[] facesArray = faces.toArray();
//        for (int i = 0; i <facesArray.length; i++)
//            Imgproc.rectangle(aInputFrame, facesArray[i].tl(), facesArray[i].br(), new Scalar(0, 255, 0, 255), 3);
//
//        return aInputFrame;
//    }

//    @Override
//    public void onResume() {
//        super.onResume();
//        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_6, this, mLoaderCallback);
//    }

    /**
     * Called when pointer capture is enabled or disabled for the current window.
     *
     * @param hasCapture True if the window has pointer capture.
     */
//    @Override
//    public void onPointerCaptureChanged(boolean hasCapture) {
//
//    }

    public Mat faceDetection(String fileName)
    {
//        Mat img = new Mat();
        Mat grayscaleImage = new Mat();
        Mat img = Imgcodecs.imread(fileName);
        Imgproc.cvtColor(img, grayscaleImage, Imgproc.COLOR_RGBA2RGB);
//        absoluteFaceSize = (int)(0.2 * mImageView.getHeight());

        MatOfRect faces = new MatOfRect();

        // Use the classifier to detect faces
        if (cascadeClassifier != null) {
            cascadeClassifier.detectMultiScale(grayscaleImage, faces, 1.1, 2, 0|CASCADE_SCALE_IMAGE,
//                    new Size(absoluteFaceSize, absoluteFaceSize), new Size());
                    new Size(60, 60), new Size());
        }

        Rect[] facesArray = faces.toArray();
        for (Rect aFacesArray : facesArray)
            Imgproc.rectangle(img, aFacesArray.tl(), aFacesArray.br(), new Scalar(0, 255, 0, 255), 3);

        return img;
    }

    public Bitmap ImageProcessing(String fileName)
    {
        Bitmap bitmap = null;
        absoluteFaceSize = (int)(0.2 * mImageView.getHeight());


        Bitmap bitmap_test = BitmapFactory.decodeFile(fileName);
//        Mat imageMat = new Mat();



//        int targetW = mImageView.getWidth();
//        int targetH = mImageView.getHeight();
//
//        // Get the dimensions of the bitmap
//        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
//        bmOptions.inJustDecodeBounds = true;
//        BitmapFactory.decodeFile(fileName, bmOptions);
//        int photoW = bmOptions.outWidth;
//        int photoH = bmOptions.outHeight;
//
//        // Determine how much to scale down the image
//        int scaleFactor = Math.min(photoW/targetW, photoH/targetH);
//
//        // Decode the image file into a Bitmap sized to fill the View
//        bmOptions.inJustDecodeBounds = false;
//        bmOptions.inSampleSize = scaleFactor << 1;
//        bmOptions.inPurgeable = true;
//
//        bitmap = BitmapFactory.decodeFile(fileName, bmOptions);


//        Utils.bitmapToMat(bitmap, imageMat);
        faceImg = new Mat();
        imageMat = Imgcodecs.imread(mfilePict);
        faceImg = Imgcodecs.imread(mfilePict);
        Mat grayscaleImage = new Mat();
        Imgproc.cvtColor(imageMat, grayscaleImage, Imgproc.COLOR_RGBA2RGB);

        MatOfRect faces = new MatOfRect();
        Mat image_roi = new Mat();;
        // Use the classifier to detect faces
        if (cascadeClassifier != null) {
            cascadeClassifier.detectMultiScale(grayscaleImage, faces, 1.1, 2, 0|CASCADE_SCALE_IMAGE,
                    new Size(absoluteFaceSize, absoluteFaceSize), new Size());
//                    new Size(60, 60), new Size());
        }

        Rect[] facesArray = faces.toArray();
        Rect rectCrop = new Rect();
        for (Rect aFacesArray : facesArray)
        {
            Imgproc.rectangle(imageMat, aFacesArray.tl(), aFacesArray.br(), new Scalar(0, 255, 0, 255), 3);
//            Imgproc.rectangle(imageMat, new Point(aFacesArray.x, aFacesArray.y), new Point(aFacesArray.x + aFacesArray.width, aFacesArray.y + aFacesArray.height), new Scalar(0, 255, 0, 255));
            rectCrop = new Rect(aFacesArray.x, aFacesArray.y, aFacesArray.width, aFacesArray.height);
            imageMat = new Mat(faceImg,rectCrop);
            bitmap = Bitmap.createBitmap(aFacesArray.width, aFacesArray.height,Bitmap.Config.ARGB_8888);

        }


//        if(image_roi.empty())
//            bitmap = null;
//        else {
////            bitmap = Bitmap.createBitmap(aFacesArray.width, aFacesArray.height,Bitmap.Config.ARGB_8888);
            Utils.matToBitmap(imageMat, bitmap);
//        }
        return bitmap;
    }


//    public void detect(View view)
    public void detect()
    {
//        initializeOpenCVDependencies();
//        faceImg = faceDetection(mfilePict);
//        Bitmap bm = Bitmap.createBitmap(faceImg.cols(), faceImg.rows(),Bitmap.Config.ARGB_8888);
//        Utils.matToBitmap(faceImg, bm);
//        mImageView = (ImageView) findViewById(R.id.faceDetectedView);

        pictbitmap = ImageProcessing(mfilePict);
        mImageView.setImageBitmap(pictbitmap);
        try {
            sendPhoto(pictbitmap);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void sentPict(View view)
    {
        try {
            sendPhoto(pictbitmap);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void sendPhoto(Bitmap bitmap) throws Exception {
        new UploadTask().execute(bitmap);
    }
//
    public class UploadTask extends AsyncTask<Bitmap, Void, Void>
    {

        protected Void doInBackground(Bitmap... bitmaps) {
            if (bitmaps[0] == null)
                return null;
            setProgress(0);

        Bitmap bitmap = bitmaps[0];
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream); // convert Bitmap to ByteArrayOutputStream
        InputStream in = new ByteArrayInputStream(stream.toByteArray()); // convert ByteArrayOutputStream to ByteArrayInputStream

        DefaultHttpClient httpclient = new DefaultHttpClient();
//			HttpClient httpclient = HttpClientBuilder.create().build();
        try {
            HttpPost httppost = new HttpPost("http://geco.transparansi.xyz/savetofile.php"); // server

            MultipartEntity reqEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
            reqEntity.addPart("myFile",
                    System.currentTimeMillis() + ".jpg", in);
            httppost.setEntity(reqEntity);

//				Log.i(TAG, "request " + httppost.getRequestLine());
            HttpResponse response = null;
            try {
                response = httpclient.execute(httppost);
            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
				try {
					if (response != null)
						Log.i(TAG, "response " + response.getStatusLine().toString());
				} finally {

				}
        } finally {

        }

        if (in != null) {
            try {
                in.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

            if (stream != null) {
                try {
                    stream.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

        return null;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            // TODO Auto-generated method stub
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            Toast.makeText(FaceDetection.this, R.string.uploaded, Toast.LENGTH_LONG).show();
        }


    }


}
